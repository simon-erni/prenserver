# Java Projekt-Template fuer HSLU

Dieses Projekt dient in den Modulen VSK und APPE als Template fuer Java-Projekte.
 
 - Java SE Version 1.8 (default) empfohlen
 - Build mit ANT 1.9.4 (mindestens 1.9.2 weg. JUnit Report Bug)
 - Unittests und Report mit JUnit 4.11 und Hamcrest 1.3.0
 - Codeabdeckung mit JaCoCo 0.7.2 (Java 1.8)
 - Report mit Checkstyle 5.7.0 (Java 1.8 ohne Lambdas)
 - Report mit Classycle 1.4.1 (nicht voll Java 1.8 kompatibel)
 - Report mit PMD 5.1.3
 - QS-Reporter Task 1.0.1
 - Source- und Binary-Distribution
 - Maven-kompatible Verzeichnissstruktur (weitgehend)
 - Codequalität von test-Code wird mitberücksichtigt.
 
 Neu in dieser Major-Version:
 - resource-Verzeichnisse in main- und test-Baum
 - Rename: Verzeichnis 'cfg' neu 'config'
 - Rename: Verzeichnis 'target/junit' neu 'target/test' (Buildserver!)
 - Neues Target 'integration-test': Führt *IT.java benannte Integationstests aus.
 - Compiler mit 'listfiles' für IDE-Integation (Eclipse)
 - 'run'-Target neu mit fork (wg. Security-Settings in IDEs)
 
 Maintenance/Bugfix:
 - Update JaCoCo auf Version 0.7.2
 - vmargs bei Tests wegen Linux reduziert
 - Reports per Properties für Problembehebung deaktivierbar.
 
 Version HS14 2.0.3 vom 10. Oktober 2014 / Roland Gisler