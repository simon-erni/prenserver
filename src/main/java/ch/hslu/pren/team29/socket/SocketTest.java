/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ch.hslu.pren.team29.socket;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;

/**
 *
 * @author Simon
 */
public class SocketTest {

    public static void main(String[] args) throws IOException {
        ServerSocket server = new ServerSocket(1337);
        Socket test = server.accept();
        System.out.println("Connection established with: " + test.getInetAddress() + ":" + test.getPort());
        BufferedReader reader = new BufferedReader(new InputStreamReader(test.getInputStream(),Charset.forName("UTF-8")));
        OutputStreamWriter writer = new OutputStreamWriter(test.getOutputStream(),"UTF-8");
        
        
        while (true) {
            String line = reader.readLine();
            System.out.println("Input Received : " + line);
            writer.write(line);
            writer.flush();
        }
    }
}
